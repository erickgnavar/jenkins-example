from unittest import TestCase
from foo.hello import Hello


class ExampleTest(TestCase):

    def test_hello(self):
        self.assertEquals(1, 1)

    def test_2(self):
        self.assertTrue(2 > 1)

    def test_wrong(self):
        self.assertTrue(True)


class HelloTestCase(TestCase):

    def test_add(self):
        hello = Hello()
        self.assertEqual(hello.add(1, 1), 2)
